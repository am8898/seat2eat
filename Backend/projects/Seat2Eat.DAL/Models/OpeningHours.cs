﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Seat2Eat.DAL.Models
{
    public class OpeningHours
    {
        public int ID { get; set; }

        public DayOfWeek DayOfWeek { get; set; }

        public DateTime FromHour { get; set; }

        public DateTime ToHour { get; set; }

        #region Foreign keys

        public int RestaurantID { get; set; }

        #endregion

        #region Navigation properties

        public Restaurant Restaurant { get; set; }

        #endregion
    }
}
