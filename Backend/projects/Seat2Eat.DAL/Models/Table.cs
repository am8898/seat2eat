﻿using Seat2Eat.DAL.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Seat2Eat.DAL.Models
{
    public class Table
    {
        public int ID { get; set; }

        public TableType TableType { get; set; }

        [Required]
        public int Number { get; set; }

        [Required]
        public int Capacity { get; set; }

        [Required]
        public int ReservedSeats { get; set; }

        #region Foreign properties

        [Required]
        public int RestaurantID { get; set; }

        #endregion
    }
}