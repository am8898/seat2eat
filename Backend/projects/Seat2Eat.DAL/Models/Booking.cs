﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Seat2Eat.DAL.Models
{
    public class Booking
    {
        public int ID { get; set; }

        [Required]
        public int SeatCount { get; set; }

        [Required]
        public DateTime From { get; set; }

        [Required]
        public DateTime To { get; set; }

        [MaxLength(255)]
        public string SpecialWishes { get; set; }

        #region Foreign keys

        public int TableID { get; set; }
        public int RestaurantID { get; set; }
        public int UserID { get; set; }

        #endregion

        #region Navigation properties

        public Table Table { get; set; }
        public Restaurant Restaurant { get; set; }
        public User User { get; set; }

        #endregion

    }


}