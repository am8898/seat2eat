﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Seat2Eat.DAL.Models
{
    public class User
    {
        public int ID { get; set; }

        [MaxLength(64)]
        [Required]
        public string Name { get; set; }

        [MaxLength(64)]
        public string Surname { get; set; }

        [MaxLength(255)]
        [Required]
        public string Username { get; set; }

        [MinLength(6)]
        [MaxLength(100)]
        public string PasswordSalt { get; set; }

        [MinLength(6)]
        [MaxLength(100)]
        public string PasswordHash { get; set; }

        #region Navigation properties

        public ICollection<UserRole> UserRoles { get; set; }
        public ICollection<Booking> Bookings { get; set; }

        #endregion

    }


}