﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Seat2Eat.DAL.Models
{
    public class Restaurant
    {
        public int ID { get; set; }

        [Required]
        [MaxLength(64)]
        public string Name { get; set; }

        #region Navigation properties

        public ICollection<Table> Tables { get; set; }
        public ICollection<UserRole> UserRoles { get; set; }
        public ICollection<Booking> Bookins { get; set; }
        public ICollection<OpeningHours> OpeningHours { get; set; }

        #endregion
    }


}