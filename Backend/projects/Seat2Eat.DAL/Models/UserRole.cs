﻿using Seat2Eat.DAL.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Seat2Eat.DAL.Models
{
    public class UserRole
    {
        public int ID { get; set; }

        public UserRoleType UserRoleType { get; set; }

        #region Foreign keys

        [Required]
        public int UserID { get; set; }

        [Required]
        public int RestaurantID { get; set; }

        #endregion

        #region Navigation properties

        public User User { get; set; }
        public Restaurant Restaurant { get; set; }

        #endregion
    }
}