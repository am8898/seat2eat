﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Seat2Eat.DAL.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Restaurants",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(maxLength: 64, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Restaurants", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(maxLength: 64, nullable: false),
                    Surname = table.Column<string>(maxLength: 64, nullable: true),
                    Username = table.Column<string>(maxLength: 255, nullable: false),
                    PasswordSalt = table.Column<string>(maxLength: 100, nullable: true),
                    PasswordHash = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "OpeningHours",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    DayOfWeek = table.Column<int>(nullable: false),
                    FromHour = table.Column<DateTime>(nullable: false),
                    ToHour = table.Column<DateTime>(nullable: false),
                    RestaurantID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OpeningHours", x => x.ID);
                    table.ForeignKey(
                        name: "FK_OpeningHours_Restaurants_RestaurantID",
                        column: x => x.RestaurantID,
                        principalTable: "Restaurants",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Tables",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    TableType = table.Column<int>(nullable: false),
                    Number = table.Column<int>(nullable: false),
                    Capacity = table.Column<int>(nullable: false),
                    ReservedSeats = table.Column<int>(nullable: false),
                    RestaurantID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tables", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Tables_Restaurants_RestaurantID",
                        column: x => x.RestaurantID,
                        principalTable: "Restaurants",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserRole",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserRoleType = table.Column<int>(nullable: false),
                    UserID = table.Column<int>(nullable: false),
                    RestaurantID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRole", x => x.ID);
                    table.ForeignKey(
                        name: "FK_UserRole_Restaurants_RestaurantID",
                        column: x => x.RestaurantID,
                        principalTable: "Restaurants",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserRole_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Bookings",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    SeatCount = table.Column<int>(nullable: false),
                    From = table.Column<DateTime>(nullable: false),
                    To = table.Column<DateTime>(nullable: false),
                    SpecialWishes = table.Column<string>(maxLength: 255, nullable: true),
                    TableID = table.Column<int>(nullable: false),
                    RestaurantID = table.Column<int>(nullable: false),
                    UserID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bookings", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Bookings_Restaurants_RestaurantID",
                        column: x => x.RestaurantID,
                        principalTable: "Restaurants",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Bookings_Tables_TableID",
                        column: x => x.TableID,
                        principalTable: "Tables",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Bookings_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Restaurants",
                columns: new[] { "ID", "Name" },
                values: new object[] { 1, "School Canteen" });

            migrationBuilder.InsertData(
                table: "Restaurants",
                columns: new[] { "ID", "Name" },
                values: new object[] { 2, "Canteena Mexicana" });

            migrationBuilder.InsertData(
                table: "Restaurants",
                columns: new[] { "ID", "Name" },
                values: new object[] { 3, "Sir Williams Pub" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "ID", "Name", "PasswordHash", "PasswordSalt", "Surname", "Username" },
                values: new object[] { 1, "Peter", null, null, null, "ptr" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "ID", "Name", "PasswordHash", "PasswordSalt", "Surname", "Username" },
                values: new object[] { 2, "Nejc", null, null, null, "nejc" });

            migrationBuilder.InsertData(
                table: "Tables",
                columns: new[] { "ID", "Capacity", "Number", "ReservedSeats", "RestaurantID", "TableType" },
                values: new object[] { 1, 3, 4, 0, 2, 0 });

            migrationBuilder.InsertData(
                table: "Tables",
                columns: new[] { "ID", "Capacity", "Number", "ReservedSeats", "RestaurantID", "TableType" },
                values: new object[] { 2, 4, 0, 0, 2, 2 });

            migrationBuilder.InsertData(
                table: "Tables",
                columns: new[] { "ID", "Capacity", "Number", "ReservedSeats", "RestaurantID", "TableType" },
                values: new object[] { 3, 4, 0, 0, 2, 1 });

            migrationBuilder.InsertData(
                table: "Tables",
                columns: new[] { "ID", "Capacity", "Number", "ReservedSeats", "RestaurantID", "TableType" },
                values: new object[] { 4, 4, 0, 0, 2, 2 });

            migrationBuilder.InsertData(
                table: "Tables",
                columns: new[] { "ID", "Capacity", "Number", "ReservedSeats", "RestaurantID", "TableType" },
                values: new object[] { 5, 8, 0, 0, 2, 1 });

            migrationBuilder.InsertData(
                table: "Bookings",
                columns: new[] { "ID", "From", "RestaurantID", "SeatCount", "SpecialWishes", "TableID", "To", "UserID" },
                values: new object[] { 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, 5, "Candles", 4, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 });

            migrationBuilder.CreateIndex(
                name: "IX_Bookings_RestaurantID",
                table: "Bookings",
                column: "RestaurantID");

            migrationBuilder.CreateIndex(
                name: "IX_Bookings_TableID",
                table: "Bookings",
                column: "TableID");

            migrationBuilder.CreateIndex(
                name: "IX_Bookings_UserID",
                table: "Bookings",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_OpeningHours_RestaurantID",
                table: "OpeningHours",
                column: "RestaurantID");

            migrationBuilder.CreateIndex(
                name: "IX_Tables_RestaurantID",
                table: "Tables",
                column: "RestaurantID");

            migrationBuilder.CreateIndex(
                name: "IX_UserRole_RestaurantID",
                table: "UserRole",
                column: "RestaurantID");

            migrationBuilder.CreateIndex(
                name: "IX_UserRole_UserID",
                table: "UserRole",
                column: "UserID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Bookings");

            migrationBuilder.DropTable(
                name: "OpeningHours");

            migrationBuilder.DropTable(
                name: "UserRole");

            migrationBuilder.DropTable(
                name: "Tables");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Restaurants");
        }
    }
}
