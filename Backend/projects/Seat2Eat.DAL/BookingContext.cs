﻿using Microsoft.EntityFrameworkCore;
using Seat2Eat.DAL.Enums;
using Seat2Eat.DAL.Models;

namespace Seat2Eat.DAL
{
    public class BookingContext : DbContext
    {
        public DbSet<Booking> Bookings { get; set; }
        public DbSet<Restaurant> Restaurants { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Table> Tables { get; set; }
        public DbSet<UserRole> UserRole { get; set; }
        public DbSet<OpeningHours> OpeningHours { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=bookings.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var sc = new Restaurant { ID = 1, Name = "School Canteen" };
            var cm = new Restaurant { ID = 2, Name = "Canteena Mexicana" };
            var swp = new Restaurant { ID = 3, Name = "Sir Williams Pub" };

            modelBuilder.Entity<Restaurant>().HasData(sc, cm, swp);

            var t1 = new Table { ID = 1, Number = 4, RestaurantID = cm.ID, Capacity = 3, TableType = TableType.Bar };
            var t2 = new Table { ID = 2, RestaurantID = cm.ID, Capacity = 4, TableType = TableType.Small };
            var t3 = new Table { ID = 3, RestaurantID = cm.ID, Capacity = 4, TableType = TableType.Big };
            var t4 = new Table { ID = 4, RestaurantID = cm.ID, Capacity = 4, TableType = TableType.Small };
            var t5 = new Table { ID = 5, RestaurantID = cm.ID, Capacity = 8, TableType = TableType.Big };

            modelBuilder.Entity<Table>().HasData(t1, t2, t3, t4, t5);

            var u1 = new User { ID = 1, Name = "Peter", Username = "ptr" };
            var u2 = new User { ID = 2, Name = "Nejc", Username = "nejc" };

            modelBuilder.Entity<User>().HasData(u1, u2);

            var b1 = new Booking { ID =1, RestaurantID = cm.ID, SeatCount = 5, UserID = u1.ID, TableID = t1.Number, SpecialWishes = "Candles" };

            modelBuilder.Entity<Booking>().HasData(b1);
        }
    }
}