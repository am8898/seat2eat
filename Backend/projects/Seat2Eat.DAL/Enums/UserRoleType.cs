﻿namespace Seat2Eat.DAL.Enums
{
    public enum UserRoleType {
        Customer, Employee
    }
}