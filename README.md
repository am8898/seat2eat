# Seat 2 Eat

## Authors: Simon Prosenik, Peter Vitéz, Marcel Mráz, Nejc Prijatelj, Erik Strnad, Andrej Mihorič

Popular restaurants often have difficulties handling larger amounts of customers, or serving customers with special needs. This project is meant to solve this problem by developing software solutions that would provide customers with simpler and more customizable experience for booking tables, while providing easier way of managing bookings and reducing expenses for restaurants. Ultimately this should simplify lives of employees too.